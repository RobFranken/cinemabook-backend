const neo4j = require('../config/neo4j');
const filmController = require('../controllers/filmController');

exports.addFilm = function (req, res) {
    const id = req.body.id;
    const title = req.body.title;
    const year = req.body.year;
    const directors = req.body.directors;
    const description = req.body.description;
    const actors = req.body.actors;
    const posterImage = req.body.posterImage;
    const backgroundImage = req.body.backgroundImage;

    filmController.addFilm(neo4j.getSession(req), id, title, year, directors, description, actors, posterImage, backgroundImage)
        .then((results) => {
            res.status(201);
            res.json(results);
        }).catch((error) => {
            res.status(400);
            res.json(error);
    });
};

exports.getFilms = function(req, res) {
    filmController.getFilms(neo4j.getSession(req))
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
            res.status(500);
            res.json(error);
    });
};

exports.getFilm = function(req, res) {
    const filmId = req.params.id;

    filmController.getFilm(neo4j.getSession(req), filmId)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
            res.status(500);
            res.json(error);
    });
};
