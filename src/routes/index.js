exports.authRoutes = require('./authRoutes');
exports.filmRoutes = require('./filmRoutes');
exports.userRoutes = require('./userRoutes');
exports.reviewRoutes = require('./reviewRoutes');
exports.commentRoutes = require('./commentRoutes');
exports.miscRoutes = require('./miscRoutes');
