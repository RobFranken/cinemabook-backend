const authController = require('../controllers/authController');
const neo4j = require('../config/neo4j');

exports.register = function(req, res) {
    const username = req.body.username;
    const password = req.body.password;

    authController.register(neo4j.getSession(req), username, password)
        .then((results) => {
            res.status(201);
            res.json(results);
        }).catch((error) => {
            res.status(error.status);
            res.json(error);
    });
};

exports.login = function(req, res) {
    const username = req.body.username;
    const password = req.body.password;

    authController.login(neo4j.getSession(req), username, password)
        .then((user) => {
            authController.generateToken(user.properties.username)
                .then((token) => {
                    res.status(200);
                    res.json({user, token});
                }).catch((error) => {
                    res.status(500);
                    res.json(error);
            });
        }).catch((error) => {
            res.status(500);
            res.json(error);
    });
};
