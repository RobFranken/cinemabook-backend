const neo4j = require('../config/neo4j');
const commentController = require('../controllers/commentController');

exports.addComment = function (req, res) {
    const reviewId = req.params.id;
    const username = req.body.username;
    const comment = req.body.comment;
    const date = req.body.date;

    commentController.addComment(neo4j.getSession(req), username, reviewId, comment, date)
        .then((results) => {
            res.status(201);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};

exports.getComments = function(req, res) {
    const reviewId = req.params.id;

    commentController.getComments(neo4j.getSession(req), reviewId)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
            res.status(500);
            res.json(error);
    });
};

exports.updateComment = function(req, res) {
  const commentId = req.params.id;
  const comment = req.body.comment;

  commentController.updateComment(neo4j.getSession(req), commentId, comment)
      .then((results) => {
          res.status(200);
          res.json(results);
      }).catch((error) => {
          res.status(400);
          res.json(error);
  });
};

exports.deleteComment = function(req, res) {
    const commentId = req.params.id;

    commentController.deleteComment(neo4j.getSession(req), commentId)
        .then((results) => {
            res.status(204);
            res.send();
        }).catch((error) => {
            res.status(500);
            res.json(error);
    });
};