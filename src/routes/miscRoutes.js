const neo4j = require('../config/neo4j');
const miscController = require('../controllers/miscController');

exports.markWatched = function (req, res) {
    const filmId = req.params.id;
    const username = req.body.username;

    miscController.markWatched(neo4j.getSession(req), username, filmId)
        .then((results) => {
            res.status(201);
            res.json(results);
        }).catch((error) => {
        res.status(400);
        res.json(error);
    });
};

exports.getWatched = function (req, res) {
    const filmId = req.params.id;
    const username = req.params.username;

    miscController.getWatched(neo4j.getSession(req), username, filmId)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(400);
        res.json(error);
    });
};

exports.removeWatched = function (req, res) {
    const watchedId = req.params.id;

    miscController.removeWatched(neo4j.getSession(req), watchedId)
        .then((results) => {
            res.status(204);
            res.send();
        }).catch((error) => {
        res.status(400);
        res.json(error);
    });
};

exports.likeFilm = function (req, res) {
    const filmId = req.params.id;
    const username = req.body.username;

    miscController.likeFilm(neo4j.getSession(req), username, filmId)
        .then((results) => {
            res.status(201);
            res.json(results);
        }).catch((error) => {
        res.status(400);
        res.json(error);
    });
};

exports.getFilmLike = function (req, res) {
    const filmId = req.params.id;
    const username = req.params.username;

    miscController.getFilmLike(neo4j.getSession(req), username, filmId)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(400);
        res.json(error);
    });
};

exports.removeFilmLike = function (req, res) {
    const likeId = req.params.id;

    miscController.removeFilmLike(neo4j.getSession(req), likeId)
        .then((results) => {
            res.status(204);
            res.send();
        }).catch((error) => {
        res.json(400);
        res.json(error);
    });
};

exports.rateFilm = function (req, res) {
    const filmId = req.params.id;
    const username = req.body.username;
    const rating = req.body.rating;

    miscController.rateFilm(neo4j.getSession(req), username, filmId, rating)
        .then((results) => {
            res.status(201);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};

exports.getRating = function (req, res) {
    const filmId = req.params.id;
    const username = req.params.username;

    miscController.getRating(neo4j.getSession(req), username, filmId)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};

exports.updateRating = function (req, res) {
    const ratingId = req.params.id;
    const filmId = req.body.filmId;
    const rating = req.body.rating;

    miscController.updateRating(neo4j.getSession(req), ratingId, filmId, rating)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};