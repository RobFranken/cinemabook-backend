const neo4j = require('../config/neo4j');
const userController = require('../controllers/userController');

exports.getAll = function(req, res) {
    userController.getAll(neo4j.getSession(req))
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
            res.status(400);
            res.json(error);
    });
};
