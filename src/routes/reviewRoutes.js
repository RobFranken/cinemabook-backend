const neo4j = require('../config/neo4j');
const reviewController = require('../controllers/reviewController');

exports.addReview = function (req, res) {
    const username = req.body.username;
    const filmId = req.body.filmId;
    const date = req.body.date;
    const review = req.body.review;

    reviewController.addReview(neo4j.getSession(req), review, date, username, filmId)
        .then((results) => {
            res.status(201);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};

exports.updateReview = function (req, res) {
    const reviewId = req.params.id;
    const review = req.body.review;

    reviewController.updateReview(neo4j.getSession(req), reviewId, review)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(400);
        res.json(error);
    });
};

exports.deleteReview = function (req, res) {
    const reviewId = req.params.id;

    reviewController.deleteReview(neo4j.getSession(req), reviewId)
        .then((results) => {
            res.status(204).send();
        }).catch((error) => {
        res.status(400);
        res.json(error);
    });
}

exports.getFilmReviews = function (req, res) {
    const filmId = req.params.id;

    reviewController.getFilmReviews(neo4j.getSession(req), filmId)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};

exports.getUserReviews = function (req, res) {
    const username = req.params.username;

    reviewController.getUserReviews(neo4j.getSession(req), username)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};

exports.getPopularReviews = function (req, res) {
    reviewController.getPopularReviews(neo4j.getSession(req))
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};

exports.getReview = function (req, res) {
    const id = req.params.id;

    reviewController.getReview(neo4j.getSession(req), id)
        .then((results) => {
            res.status(200);
            res.json(results);
        }).catch((error) => {
        res.status(500);
        res.json(error);
    });
};
