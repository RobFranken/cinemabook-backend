require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config/config');
const neo4j = require('./config/neo4j');
const routes = require('./routes');
const auth = require('./middleware/authentication');
const cors = require('cors');

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(neo4j.sessionCleanup);


app.get('/users', auth.validateToken, routes.userRoutes.getAll);
app.post('/users', routes.authRoutes.register);
app.post('/users/login', routes.authRoutes.login);

app.get('/films', routes.filmRoutes.getFilms);
app.get('/films/:id', routes.filmRoutes.getFilm);
app.post('/films', auth.validateAdmin, routes.filmRoutes.addFilm);

app.get('/reviews', routes.reviewRoutes.getPopularReviews);
app.get('/reviews/:id', routes.reviewRoutes.getReview);
app.put('/reviews/:id', auth.validateToken, routes.reviewRoutes.updateReview);
app.delete('/reviews/:id', auth.validateToken, routes.reviewRoutes.deleteReview);
app.get('/films/:id/reviews', routes.reviewRoutes.getFilmReviews);
app.get('/:username/reviews', routes.reviewRoutes.getUserReviews);
app.post('/reviews', auth.validateToken, routes.reviewRoutes.addReview);

app.get('/reviews/:id/comments', routes.commentRoutes.getComments);
app.post('/reviews/:id/comment', auth.validateToken, routes.commentRoutes.addComment);
app.put('/comments/:id', auth.validateToken, routes.commentRoutes.updateComment);
app.delete('/comments/:id', auth.validateToken, routes.commentRoutes.deleteComment);

app.post('/films/:id/watched', auth.validateToken, routes.miscRoutes.markWatched);
app.get('/:username/:id/watched', auth.validateToken, routes.miscRoutes.getWatched);
app.delete('/watched/:id', auth.validateToken, routes.miscRoutes.removeWatched);

app.post('/films/:id/like', auth.validateToken, routes.miscRoutes.likeFilm);
app.get('/:username/:id/like', auth.validateToken, routes.miscRoutes.getFilmLike);
app.delete('/like/film/:id', auth.validateToken, routes.miscRoutes.removeFilmLike);

app.get('/:username/:id/rate', auth.validateToken, routes.miscRoutes.getRating);
app.post('/films/:id/rate', auth.validateToken, routes.miscRoutes.rateFilm);
app.put('/rate/:id', auth.validateToken, routes.miscRoutes.updateRating);

const port = process.env.PORT || config.Port;
app.listen(port, function () {
    console.log(`Server is running on port ${port}`);
});
