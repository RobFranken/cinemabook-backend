const _ = require('lodash');
const uuid = require('uuid').v4;
const neo4j = require('neo4j-driver').v1;

const markWatched = function (session, username, filmId) {
    const id = uuid();
    return session.run('MATCH (u:User {username: $username}) MATCH (f:Film {id: $filmId}) ' +
        'CREATE (u)-[w:WATCHED {id: $id}]->(f) ' +
        'SET f.watchers = f.watchers + 1 ' +
        'RETURN w', {username: username, filmId: filmId, id: id})
        .then((results) => {
            return results.records;
        });
};

const getWatched = function (session, username, filmId) {
    return session.run('MATCH (u:User {username: $username})-[w:WATCHED]->(f:Film {id: $filmId}) RETURN w', {
        username: username,
        filmId: filmId
    })
        .then((results) => {
            return results.records
        });
};

const removeWatched = function (session, watchedId) {
    return session.run('MATCH (:User)-[w:WATCHED {id: $watchedId}]->(f:Film) ' +
        'SET f.watchers = f.watchers - 1 ' +
        'DETACH DELETE w', {watchedId: watchedId});
};

const likeFilm = function (session, username, filmId) {
    const id = uuid();
    return session.run('MATCH (u:User {username: $username}) MATCH (f:Film {id: $filmId}) ' +
        'CREATE (u)-[l:LIKE {id: $id}]->(f) ' +
        'SET f.likes = f.likes + 1 ' +
        'RETURN l', {username: username, filmId: filmId, id: id})
        .then((results) => {
            return results.records;
        });
};

const getFilmLike = function (session, username, filmId) {
    return session.run('MATCH (:User {username: $username})-[l:LIKE]->(f:Film {id: $filmId}) RETURN l', {
        username: username,
        filmId: filmId
    })
        .then((results) => {
            return results.records;
        });
};

const removeFilmLike = function (session, likeId) {
    return session.run('MATCH (:User)-[l:LIKE {id: $likeId}]->(f:Film) ' +
        'SET f.likes = f.likes -1 ' +
        'DETACH DELETE l', {likeId: likeId});
};

const rateFilm = function (session, username, filmId, rating) {
    const id = uuid();

    return session.writeTransaction(tx => tx.run('MATCH (u:User {username: $username}) MATCH (f:Film {id: $filmId}) ' +
        'CREATE (u)-[r:RATED {id: $id, rating: $rating}]->(f) RETURN r', {
        username: username,
        filmId: filmId,
        id: id,
        rating: neo4j.int(rating)
    }))
        .then(() => session.writeTransaction(tx => tx.run('MATCH (f:Film {id: $filmId2})<-[r:RATED]-(:User) ' +
            'WITH f, avg(r.rating) as average ' +
            'SET f.rating = average', {filmId2: filmId})));
};

const getRating = function (session, username, filmId) {
    return session.run('MATCH (:User {username: $username})-[r:RATED]->(:Film {id: $filmId}) RETURN r', {
        username: username,
        filmId: filmId
    })
        .then((results) => {
            return results.records;
        });
};

const updateRating = function (session, ratingId, filmId, rating) {
    return session.writeTransaction(tx => tx.run('MATCH (:Film)<-[r:RATED {id: $ratingId}]-(:User) ' +
        'SET r.rating = $rating ', {ratingId: ratingId, rating: neo4j.int(rating)}))
        .then(() => session.writeTransaction(tx => tx.run('MATCH (f:Film {id: $filmId})-[r:RATED]-(:User) ' +
            'WITH f, avg(r.rating) as average ' +
            'SET f.rating = average', {filmId: filmId})));
}

module.exports = {
    markWatched,
    getWatched,
    removeWatched,
    likeFilm,
    getFilmLike,
    removeFilmLike,
    rateFilm,
    getRating,
    updateRating
};