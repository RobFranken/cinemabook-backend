const uuid = require('uuid').v4;
const _ = require('lodash');

const createReview = function (session, review, date) {
    const id = uuid();
    return session.run('CREATE (r:Review {id: $id, review: $review, date: $date, comments: 0}) RETURN r', {
        id: id,
        review: review,
        date: date
    });
};

const createRelBetweenUserAndReview = function (session, username, reviewId) {
    return session.run('MATCH (r:Review {id: $reviewId}) MATCH (u:User {username: $username}) ' +
        'CREATE (u)-[rel:MADE]->(r) RETURN rel', {reviewId: reviewId, username: username});
};

const createRelBetweenReviewAndFilm = function (session, filmId, reviewId) {
    return session.run('MATCH (r:Review {id: $reviewId}) MATCH (f:Film {id: $filmId}) ' +
        'CREATE (f)<-[rel:ON]-(r) RETURN rel', {reviewId: reviewId, filmId: filmId});
};

const addReview = function (session, review, date, username, filmId) {
    return session.run('MATCH (u:User {username: $username})-[:MADE]->(r:Review)-[:ON]->(f:Film {id: $filmId}) RETURN r', {
        username: username,
        filmId: filmId
    })
        .then((r) => {
            if (_.isEmpty(r.records[0])) {
                let reviewId;
                return session.writeTransaction(tx => createReview(tx, review, date))
                    .then((results) => {
                        reviewId = results.records[0].get(0).properties.id;
                    }).then(() => session.writeTransaction(tx => createRelBetweenUserAndReview(tx, username, reviewId)))
                    .then(() => session.writeTransaction(tx => tx.run('MATCH (f:Film {id: $filmId}) SET f.reviews = f.reviews + 1', {filmId: filmId})))
                    .then(() => session.writeTransaction(tx => createRelBetweenReviewAndFilm(tx, filmId, reviewId)));
            } else {
                throw {
                    message: 'You cannot post this review, because you have already reviewed this film.',
                    status: 400
                };
            }
        });
};

const updateReview = function(session, reviewId, review) {
    return session.run('MATCH (r:Review {id: $reviewId}) ' +
        'SET r.review = $review ' +
        'RETURN r', {reviewId: reviewId, review: review})
        .then((results) => {
            return results.records;
        });
};

const deleteReview = function(session, reviewId) {
    return session.run('MATCH (r:Review {id: $reviewId})-[:ON]->(f:Film) ' +
        'SET f.reviews = f.reviews - 1 ' +
        'DETACH DELETE r', {reviewId: reviewId});
};

const getFilmReviews = function (session, filmId) {
    return session.run('MATCH (f:Film {id: $filmId})<-[:ON]-(r:Review)<-[:MADE]-(u:User) RETURN r, u', {filmId: filmId})
        .then((results) => {
            return results.records;
        });
};

const getUserReviews = function (session, username) {
    return session.run('MATCH (u:User {username: $username})-[rel:MADE]->(r:Review)-[:ON]->(f:Film) RETURN r, f', {username: username})
        .then((results) => {
            return results.records;
        });
};

const getPopularReviews = function (session) {
    return session.run('MATCH (u:User)-[:MADE]->(r:Review)-[:ON]->(f:Film) RETURN r, u, f LIMIT 6')
        .then((results) => {
            return results.records;
        });
};

const getReview = function (session, reviewId) {
    return session.run('MATCH (u:User)-[:MADE]->(r:Review {id: $reviewId})-[:ON]->(f:Film) RETURN u, r, f', {reviewId: reviewId})
        .then((results) => {
            return results.records;
        });
};

module.exports = {
    createReview, createRelBetweenReviewAndFilm, createRelBetweenUserAndReview, addReview, getFilmReviews,
    getUserReviews, getPopularReviews, getReview, updateReview, deleteReview
};
