exports.addFilm = function(session, id, title, year, directors, description, actors, posterImage, backgroundImage) {
    return session.run('CREATE (f:Film {id: $id,' +
        ' title: $title, year: $year, directors: $directors,' +
        ' description: $description, actors: $actors, posterImage: $posterImage, backgroundImage: $backgroundImage, reviews: 0, watchers: 0, likes: 0, rating: 0})' +
        ' RETURN f',
        {id: id, title: title, year: year, directors: directors, description: description, actors: actors,
        posterImage: posterImage, backgroundImage: backgroundImage});
};

exports.getFilms = function(session) {
    return session.run('MATCH (f:Film) RETURN f')
        .then((results) => {
            return results.records;
        });
};

exports.getFilm = function(session, filmId) {
    return session.run('MATCH (f:Film {id: $filmId}) RETURN f', {filmId: filmId})
        .then((results) => {
            return results.records;
        });
};
