const uuid = require('uuid').v4;

const addComment = function (session, username, reviewId, comment, date) {
    const id = uuid();
    return session.run('MATCH (u:User {username: $username}) MATCH (r:Review {id: $reviewId}) ' +
        'CREATE (u)-[c:COMMENT {id: $id, comment: $comment, date: $date}]->(r) ' +
        'SET r.comments = r.comments + 1 ' +
        'RETURN c',
        {username: username, reviewId: reviewId, id: id, comment: comment, date: date})
        .then((results) => {
            return results.records;
        });
};

const getComments = function (session, reviewId) {
    return session.run('MATCH (u:User)-[c:COMMENT]->(r:Review {id: $reviewId}) RETURN u, c ORDER BY c.date', {reviewId: reviewId})
        .then((results) => {
            return results.records;
        });
};

const updateComment = function (session, commentId, comment) {
    return session.run('MATCH (:User)-[c:COMMENT {id: $commentId}]->(:Review) ' +
        'SET c.comment = $comment ' +
        'RETURN c', {commentId: commentId, comment: comment});
};

const deleteComment = function (session, commentId) {
    return session.run('MATCH (:User)-[c:COMMENT {id: $commentId}]->(r:Review) ' +
        'SET r.comments = r.comments - 1 ' +
        'DETACH DELETE c', {commentId: commentId});
};

module.exports = {addComment, getComments, deleteComment, updateComment};