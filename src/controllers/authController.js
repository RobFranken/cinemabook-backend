const bcrypt = require('bcrypt');
const _ = require('lodash');
const jwt = require('jsonwebtoken');

exports.register = function(session, username, password) {
    return session.run('MATCH (u:User {username: $username}) RETURN u', {username: username})
        .then((results) => {
            if (_.isEmpty(results.records[0])) {
                return session.writeTransaction(tx => bcrypt.hash(password, 10))
                    .then((hash) => session.writeTransaction(tx => tx.run('CREATE (u:User {username: $username, password: $hash}) RETURN u', {username: username, hash: hash})));
            } else {
                throw {message: 'Username is already taken. Please try another username.', status: 400};
            }
        });
};

exports.login = function(session, username, password) {
    return session.run('MATCH (u:User {username: $username}) RETURN u', {username: username})
        .then((results) => {
            if (!_.isEmpty(results.records[0])) {
                const user = results.records[0].get(0);
                return bcrypt.compare(password, user.properties.password)
                    .then((res) => {
                        if (res === true) {
                            return user;
                        } else {
                            throw {message: 'Passwords do not match. Please enter the correct password.', status: 401};
                        }
                    });
            } else {
                throw {message: 'User not found. Please enter an existing user.', status: 404};
            }
        });
};

exports.generateToken = function(username) {
    return new Promise(function(resolve, reject) {
        resolve(jwt.sign({username: username}, process.env.JWT_SECRET_KEY, {expiresIn: '3 days'}));
    });
};
