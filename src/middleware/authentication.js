const jwt = require('jsonwebtoken');
const neo4j = require('../config/neo4j');
const _ = require('lodash');

exports.validateToken = function (req, res, next) {
    let token = req.header('Authorization');

    if (token) {
        token = token.replace('Bearer ', '');
        jwt.verify(token, process.env.JWT_SECRET_KEY, function (error, decoded) {
            if (error) {
                res.status(401).json(error);
            } else {
                const session = neo4j.getSession(req);
                session.run('MATCh (u:User {username: $username}) RETURN u', {username: decoded.username})
                    .then((results) => {
                        if (!_.isEmpty(results.records[0].get(0))) {
                            next();
                        } else {
                            res.status(401).json({message: 'Invalid token'});
                        }
                    });
            }
        });
    } else {
        res.status(401).json({message: 'Unauthorized. No token provided.'});
    }
};

exports.validateAdmin = function (req, res, next) {
    let token = req.header('Authorization');

    if (token) {
        token = token.replace('Bearer ', '');
        jwt.verify(token, process.env.JWT_SECRET_KEY, function(error, decoded) {
            if (error) {
                res.status(401).json(error);
            } else {
                if (decoded.username === 'admin') {
                    next();
                } else {
                    res.status(401).json({message: 'Only admin can do this.'});
                }
            }
        });
    } else {
        res.status(401).json({message: 'Token is required'});
    }
};
