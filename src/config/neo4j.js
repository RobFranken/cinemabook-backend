const neo4j = require('neo4j-driver').v1;
const config = require('../config/config');

const neo4jConnectionString = process.env.NEO4J_CONNECTION_STRING;

const driver = neo4j.driver(neo4jConnectionString, neo4j.auth.basic('admin', 'b.ngOJGPhv1zA3.bbI7vCRNqP18NhIq'));

exports.getSession = function(context) {
    if (context.neo4jSession) {
        return context.neo4jSession;
    } else {
        context.neo4jSession = driver.session();
        return context.neo4jSession;
    }
};

exports.sessionCleanup = function(req, res, next) {
    res.on('finish', function() {
        if (req.neo4jSession) {
            req.neo4jSession.close();
            delete req.neo4jSession;
        }
    });
    next();
};
