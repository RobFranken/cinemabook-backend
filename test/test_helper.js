require('dotenv').config();
const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver('bolt://localhost:7687', neo4j.auth.basic('neo4j', 'neo4j2'));
const session = driver.session();

beforeEach(function(done) {
    session.run('CALL ga.resttest.clearDatabase();').then(() => {
        session.close();
        done();
    });
});
