const assert = require('assert');
const authController = require('../src/controllers/authController');
const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver('bolt://localhost:7687', neo4j.auth.basic('neo4j', 'neo4j2'));
const jwt = require('jsonwebtoken');
const config = require('../src/config/config');

const bcrypt = require('bcrypt');

describe('Registering users', function() {
    it('Registers a user', function(done) {
       const user1 = {
           username: 'Test',
           password: 'test123'
       };

       const session = driver.session();
       authController.register(session, user1.username, user1.password)
           .then((results) => {
               bcrypt.compare(user1.password, results.records[0].get(0).properties.password).then((res) => {
                   assert(res === true);
                   assert(results.records[0].get(0).properties.username === 'Test');
                   session.close();
                   done();
               });
           });
    });

    it('Fails to register a user, because the username is already taken.',function(done) {
        const user1 = {
            username: 'Test',
            password: 'test123'
        };
        const user2 = {
            username: 'Test',
            password: 'test123'
        };

        const session = driver.session();
        authController.register(session, user1.username, user1.password)
            .then(() => {
                authController.register(session, user2.username, user2.password)
                    .catch((error) => {
                        assert(error.message === 'Username is already taken. Please try another username.');
                        session.close();
                        done();
                    });
            });
    });
});

describe('Logging in', function() {
    it('Returns a user, meaning the user was found and the passwords match', function (done) {
        const user1 = {
            username: 'Test',
            password: 'test123'
        };

        const session = driver.session();
        authController.register(session, user1.username, user1.password)
            .then(() => {
                authController.login(session, user1.username, user1.password)
                    .then((results) => {
                        assert(results.properties.username === 'Test');
                        session.close();
                        done();
                    });
            });
    });

    it('Fails to log in, because the user was not found', function(done) {
        const session = driver.session();
        authController.login(session, 'Test', 'test123')
            .catch((error) => {
                assert(error.message === 'User not found. Please enter an existing user.');
                assert(error.status === 404);
                session.close();
                done();
            });
    });

    it('Fails to log in, because the passwords do not match', function(done) {
        const user1 = {
            username: 'Test',
            password: 'test123'
        };

        const session = driver.session();
        authController.register(session, user1.username, user1.password)
            .then(() => {
                authController.login(session, user1.username, 'test12')
                    .catch((error) => {
                        assert(error.message === 'Passwords do not match. Please enter the correct password.');
                        assert(error.status === 401);
                        session.close();
                        done();
                    });
            });
    });

    it('Generates a valid jwt token', function (done) {
        authController.generateToken('Test')
            .then((token) => {
                jwt.verify(token, config.JWT_SECRET_KEY, function(err, decoded) {
                    assert(decoded.username === 'Test');
                    done();
                });
            });
    });
});
