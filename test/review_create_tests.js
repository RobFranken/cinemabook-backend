const assert = require('assert');
const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver('bolt://localhost:7687', neo4j.auth.basic('neo4j', 'neo4j2'));
const authController = require('../src/controllers/authController');
const filmController = require('../src/controllers/filmController');
const reviewController = require('../src/controllers/reviewController');

describe('Creating reviews', function () {
    it('Should create a review', function (done) {
        const session = driver.session();
        reviewController.createReview(session, 'Test Review', '2019-12-1')
            .then((results) => {
                assert(results.records[0].get(0).properties.review === 'Test Review');
                session.close();
                done();
            });
    });

    it('Saves the relationship between review and user', function (done) {
        const session = driver.session();

        authController.register(session, 'Test', 'test123')
            .then(() => {
                reviewController.createReview(session, 'Test Review', '2019-12-2')
                    .then((results) => {
                        reviewController.createRelBetweenUserAndReview(session, 'Test', results.records[0].get(0).properties.id)
                            .then((rel) => {
                                assert(rel.records[0].get(0).type === 'MADE');
                                session.close();
                                done();
                            });
                    });
            });
    });

    it('Saves the relationship between review and film', function (done) {
        const session = driver.session();

        filmController.addFilm(session, 1, 'Test', 2003, 'test', 'test', 'test', 'test', 'test')
            .then(() => {
                reviewController.createReview(session, 'Test', '2019-12-2')
                    .then((review) => {
                        reviewController.createRelBetweenReviewAndFilm(session, 1, review.records[0].get(0).properties.id)
                            .then((results) => {
                                assert(results.records[0].get(0).type === 'ON');
                                session.close();
                                done();
                            });
                    });
            });
    });

    it('Should do these things in a transaction', function (done) {
        const session = driver.session();
        authController.register(session, 'Test', 'test123')
            .then(() => {
                filmController.addFilm(session, '1', 'Test', '2002', 'test', 'test', 'test', 'test', 'test')
                    .then(() => {
                        reviewController.addReview(session, 'Test', '2019-12-2', 'Test', '1')
                            .then((results) => {
                                assert(results.records[0].get(0).type === 'ON');
                                session.close();
                                done();
                            });
                    });
            });
    });

    it('Should not save the review, because the user has already reviewed this film', function (done) {
        const session = driver.session();
        authController.register(session, 'Test', 'test123')
            .then(() => {
                filmController.addFilm(session, '1', 'Test', '2002', 'test', 'test', 'test', 'test', 'test')
                    .then(() => {
                        reviewController.addReview(session, 'Test', '2019-12-2', 'Test', '1')
                            .then(() => {
                                reviewController.addReview(session, 'Test Review', '2019-12-1', 'Test', '1')
                                    .catch((error) => {
                                        assert(error.message === 'You cannot post this review, because you have already reviewed this film.');
                                        session.close();
                                        done();
                                    });
                            });
                    });
            });
    });
});
